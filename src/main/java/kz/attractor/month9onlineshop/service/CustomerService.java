package kz.attractor.month9onlineshop.service;

import kz.attractor.month9onlineshop.dto.CustomerResponseDTO;
import kz.attractor.month9onlineshop.exception.CustomerAlreadyRegisteredException;
import kz.attractor.month9onlineshop.exception.CustomerNotFoundException;
import kz.attractor.month9onlineshop.model.Customer;
import kz.attractor.month9onlineshop.model.CustomerRegisterForm;
import kz.attractor.month9onlineshop.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;

    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }

    public CustomerResponseDTO getByEmail(String email) {
        var user = repository.findByEmail(email);

        return CustomerResponseDTO.from(user);
    }
}
