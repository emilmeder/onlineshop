package kz.attractor.month9onlineshop.service;
import kz.attractor.month9onlineshop.dto.*;
import kz.attractor.month9onlineshop.exception.ResourceNotFoundException;
import kz.attractor.month9onlineshop.model.*;
import kz.attractor.month9onlineshop.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CartService {
    private final CartRepository cr;
    private final ProductCartRepository pcr;
    private final CustomerRepository customerRepository;
    private final ProductRepository pr;
    List<Product> products = new ArrayList<>();


    public void addCart(Authentication authentication, int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var product = pr.findProductById(id);
        var user = customerRepository.findByEmail(userDetails.getUsername());
        if(!cr.existsById(user.getId())) {
            Cart cart = Cart.builder().customer(user).session("123").build();
            cr.save(cart);
        }

        var productCart = ProductCart.builder().cart(cr.findByCustomerId(user.getId())).product(product).qty(1).build();
        pcr.save(productCart);
    }


    public Page<ProductDto> getAllCartProducts(Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = customerRepository.findByEmail(userDetails.getUsername());
        var cart = cr.findByCustomerId(user.getId());
        products.clear();

        List<ProductCart> cartProductsList = pcr.findAllByCartId(cart.getId());
        for (int i = 0; i < cartProductsList.size(); i++){

            products.add(pr.findById(cartProductsList.get(i).getId()).get());
        }
        Page<Product> productPage = new PageImpl<>(products);
        return productPage.map(ProductDto::from);
    }

    public Page<ProductDto> order(){
        Page<Product>product = new PageImpl<>(products);
        return product.map(ProductDto::from);

    }

    public void remove(Authentication authentication,int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var customer = customerRepository.findByEmail(userDetails.getUsername());
        var cart = cr.findByCustomerId(customer.getId());
        var productCart = pcr.findByProductAndAndCartId(cart.getId(),id);
        pcr.delete(productCart);

    }
}
