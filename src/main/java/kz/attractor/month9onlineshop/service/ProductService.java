package kz.attractor.month9onlineshop.service;

import kz.attractor.month9onlineshop.dto.*;
import kz.attractor.month9onlineshop.exception.ResourceNotFoundException;
import kz.attractor.month9onlineshop.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductService {
    private final ProductRepository pr;

    public Page<ProductDto> getProducts(int type_id, Pageable pageable){
        return pr.findAllByTypeId(type_id,pageable)
                .map(ProductDto::from);
    }
    public Page<ProductDto> getFindProduct(String name,Pageable pageable){
        return pr.findProductsByName(name, pageable)
                .map(ProductDto::from);
    }

    public Page<ProductDto> getProducts(Pageable pageable) {
        return pr.findAll(pageable)
                .map(ProductDto::from);
        //.toList();
    }
    public ProductDto getProductById(int id){
        var product = pr.findProductById(id);
        ProductDto productDto = ProductDto.from(product);
        return productDto;
    }

    public ProductDto getProducts(int id) {
        var place = pr.findById(id).orElseThrow(() -> new ResourceNotFoundException("place", id));
        return ProductDto.from(place);
    }
}