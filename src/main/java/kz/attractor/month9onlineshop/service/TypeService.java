package kz.attractor.month9onlineshop.service;

import kz.attractor.month9onlineshop.dto.*;
import kz.attractor.month9onlineshop.model.Product;
import kz.attractor.month9onlineshop.repository.ProductRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TypeService {
    private final ProductRepository pr;

    public Page<ProductDto> getProduct(int id, Pageable pageable) {
        return pr.findAllByTypeId(id, pageable)
                .map(ProductDto::from);
    }
}
