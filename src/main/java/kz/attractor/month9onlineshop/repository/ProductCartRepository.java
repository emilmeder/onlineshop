package kz.attractor.month9onlineshop.repository;

import kz.attractor.month9onlineshop.model.Cart;
import kz.attractor.month9onlineshop.model.ProductCart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductCartRepository extends JpaRepository<ProductCart, Integer> {
    public List<ProductCart> findAllByCartId(int id);

    public ProductCart findByProductAndAndCartId(int id1, int id2);
}
