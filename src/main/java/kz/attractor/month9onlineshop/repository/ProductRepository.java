package kz.attractor.month9onlineshop.repository;

import kz.attractor.month9onlineshop.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    Page<Product> findAllByTypeId(int typeId, Pageable pageable);

    Page<Product> findProductsByName(String name, Pageable pageable);

    Product findProductById(int id);
}
