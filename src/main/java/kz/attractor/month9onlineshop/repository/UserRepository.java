package kz.attractor.month9onlineshop.repository;

import kz.attractor.month9onlineshop.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
