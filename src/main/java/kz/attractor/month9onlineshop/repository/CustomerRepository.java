package kz.attractor.month9onlineshop.repository;

import kz.attractor.month9onlineshop.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    boolean existsByEmail(String email);

//    Optional<Customer> findByEmail(String email);

    Customer findByEmail(String email);
}
