package kz.attractor.month9onlineshop.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Column(length = 128)
    private String name;
    @NotBlank
    @Column(length = 128)
    private String mail;
    @NotBlank
    @Column(length = 128)
    private String password;
    @NotBlank
    @Column(length = 128)
    private String login;

}
