//package kz.attractor.month9onlineshop.model;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name = "brand")
//public class Brand {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//    @Column(length = 128)
//    private String name;
//
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "brand")
//    @OrderBy("name ASC")
//    List<Product> productList;
//}