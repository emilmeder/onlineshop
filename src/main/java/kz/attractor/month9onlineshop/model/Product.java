package kz.attractor.month9onlineshop.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String image;

    @Column(length = 128)
    private String description;
    @NotBlank
    @Column(length = 128)
    private Integer qty;
    @NotBlank
    @Column
    private double price;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;

//    @ManyToOne
//    @JoinColumn(name = "brand_id")
//    private Brand brand;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "product")
    Set<ProductCart> productCartList;


}