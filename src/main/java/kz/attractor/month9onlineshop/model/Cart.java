package kz.attractor.month9onlineshop.model;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String session;

    @ManyToOne
    @JoinColumn(name = "customers_id")
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "cart")
    Set<ProductCart> productCartList;
}
