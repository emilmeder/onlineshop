package kz.attractor.month9onlineshop.controller;


import kz.attractor.month9onlineshop.dto.ProductDto;
import kz.attractor.month9onlineshop.repository.ProductRepository;
import kz.attractor.month9onlineshop.repository.TypeRepository;
import kz.attractor.month9onlineshop.service.ProductService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MainController {
    ProductService ps;
    @Autowired
    ProductRepository pr;
    @Autowired
    TypeRepository tr;

//    @RequestMapping("/")
//    public String root(Model model) {
//        model.addAttribute("product", pr.findAll());
//        pr.findAll().forEach(product -> System.out.println(product));
//        return "index";
//    }

    @GetMapping("/find/{find}")
    public List<ProductDto> find(
    @PathVariable("find")
        String find, Pageable pageable){
        return ps.getFindProduct(find,pageable).getContent();
        }


}




