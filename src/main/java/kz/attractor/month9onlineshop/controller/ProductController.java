package kz.attractor.month9onlineshop.controller;

import kz.attractor.month9onlineshop.dto.ProductDto;
import kz.attractor.month9onlineshop.service.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Controller
@RequestMapping("/index")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductController {
    private final ProductService ps;
    private final PropertiesService propertiesService;

    @GetMapping
    public String mainPage() {
        return "index";
    }


    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }
        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping("/{id}")
    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder,@PathVariable("id")int id) {
        var products = ps.getProducts(id, pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(products, propertiesService.getDefaultPageSize(), model, uri);
        return "watches";
    }

    //    @GetMapping("/product/{id}")
//    public List<ProductDto> getProduct(
//            @PathVariable("id")int id,
//            Pageable pageable){
//        return ps.getProducts(id,pageable).getContent();
//    }
    @GetMapping("/product/{id}")
    public String product(Model model, @PathVariable("id")int id){
        var product = ps.getProductById(id);
        model.addAttribute("product",product);
        return "productonly";
    }


}