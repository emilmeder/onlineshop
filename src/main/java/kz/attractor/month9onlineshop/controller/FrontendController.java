//package kz.attractor.month9onlineshop.controller;
//
//import kz.attractor.month9onlineshop.exception.ResourceNotFoundException;
//import kz.attractor.month9onlineshop.service.ProductService;
//import kz.attractor.month9onlineshop.service.PropertiesService;
//import kz.attractor.month9onlineshop.service.TypeService;
//import lombok.AllArgsConstructor;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//
//@Controller
//@RequestMapping
//@AllArgsConstructor
//public class FrontendController {
//
//    private final ProductService ps;
//    private final TypeService ts;
//
//    private final PropertiesService propertiesService;
//
//    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
//        if (list.hasNext()) {
//            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
//        }
//
//        if (list.hasPrevious()) {
//            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
//        }
//
//        model.addAttribute("hasNext", list.hasNext());
//        model.addAttribute("hasPrev", list.hasPrevious());
//        model.addAttribute("items", list.getContent());
//        model.addAttribute("defaultPageSize", pageSize);
//    }
//
//    private static String constructPageUri(String uri, int page, int size) {
//        return String.format("%s?page=%s&size=%s", uri, page, size);
//    }
//
//    @GetMapping
//    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder) {
//        var places = ps.getProducts(pageable);
//        var uri = uriBuilder.getRequestURI();
//        constructPageable(places, propertiesService.getDefaultPageSize(), model, uri);
//
//        return "index";
//    }
//
//    @GetMapping("/places/{id:\\d+?}")
//    public String placePage(@PathVariable int id, Model model, Pageable pageable, HttpServletRequest uriBuilder) {
//        model.addAttribute("place", ps.getProducts(id));
//        var uri = uriBuilder.getRequestURI();
//        var foods = ts.getProduct(id, pageable);
//        constructPageable(foods, propertiesService.getDefaultPageSize(), model, uri);
//
//        return "type";
//    }
//
//    @ExceptionHandler(ResourceNotFoundException.class)
//    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
//    private String handleRNF(ResourceNotFoundException ex, Model model) {
//        model.addAttribute("resource", ex.getResource());
//        model.addAttribute("id", ex.getId());
//        return "resource-not-found";
//    }
//
//}
