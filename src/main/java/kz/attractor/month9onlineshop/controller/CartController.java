package kz.attractor.month9onlineshop.controller;
import kz.attractor.month9onlineshop.model.CustomerRegisterForm;
import kz.attractor.month9onlineshop.model.User;
import kz.attractor.month9onlineshop.repository.UserRepository;
import kz.attractor.month9onlineshop.service.*;
import kz.attractor.month9onlineshop.service.CustomerService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CartController {
    private final CartService cartService;
    private final PropertiesService propertiesService;

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }
        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping("/cart")
    public String cart(Authentication authentication, Model model, HttpServletRequest uriBuilder) {
        var products = cartService.getAllCartProducts(authentication);
        var uri = uriBuilder.getRequestURI();
        constructPageable(products,propertiesService.getDefaultPageSize(),model,uri);
        return "cart";
    }

    @PostMapping("/cart/add")
    public String addCart(Authentication authentication,@RequestParam("id") int id){
        cartService.addCart(authentication,id);
        return "redirect:/cart";
    }

    @GetMapping("/order")
    public String order(Model model){
        model.addAttribute("products",cartService.order().getContent());
        return "order";
    }

    @PostMapping("/cart/remove")
    public String remove(Authentication authentication, @RequestParam("product_id")int id){
        cartService.remove(authentication, id);
        return "redirect:/cart";
    }

}
