package kz.attractor.month9onlineshop.controller;

import kz.attractor.month9onlineshop.model.Customer;
import kz.attractor.month9onlineshop.model.CustomerRegisterForm;
import kz.attractor.month9onlineshop.model.PasswordResetToken;
import kz.attractor.month9onlineshop.repository.CustomerRepository;
import kz.attractor.month9onlineshop.repository.ResetRepository;
import kz.attractor.month9onlineshop.repository.UserRepository;
import kz.attractor.month9onlineshop.service.CustomerService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.UUID;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginController {
    public final UserRepository ur;
    public final CustomerService cs;
    public final CustomerRepository customerRepository;
    public final ResetRepository resetRepo;




    @GetMapping("/register")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }

        return "register";
    }

    @PostMapping("/register")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/login";
        }

        cs.register(customerRequestDto);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {

            session.invalidate();
        }

        return "redirect:/index";
    }

    @GetMapping("/forgot-password")
    public String pageForgotPassword(Model model) {
        return "forgot";
    }

    @PostMapping("/forgot-password")
    public String submitForgotPasswordPage(@RequestParam("email") String email,
                                           RedirectAttributes attributes) {

        if (!customerRepository.existsByEmail(email)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/index";
        }

        PasswordResetToken pToken = PasswordResetToken.builder()
                .customer(customerRepository.findByEmail(email))
                .token(UUID.randomUUID().toString())
                .build();

        resetRepo.deleteAll();
        resetRepo.save(pToken);

        return "redirect:/forgot-success";
    }

    @GetMapping("/forgot-success")
    public String pageResetPassword(Model model) {
        return "forgot-success";
    }

    @PostMapping("/reset-password")
    public String submitResetPasswordPage(@RequestParam("token") String token,
                                          @RequestParam("newPassword") String newPassword,
                                          RedirectAttributes attributes) {

        if (!resetRepo.existsByToken(token)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/reset-password";
        }

        PasswordResetToken pToken = resetRepo.findByToken(token).get();
        Customer customer = customerRepository.findById(pToken.getCustomer().getId()).get();
        customer.setPassword(new BCryptPasswordEncoder().encode(newPassword));

        customerRepository.save(customer);

        return "redirect:/login";
    }

//    @GetMapping("/register")
//    public String register(){
//
//        return "reg";
//    }
//    @GetMapping("/login")
//    public String login(){
//
//        return "log";
//    }
//
//    @PostMapping("/register")
//    public String makeUser(
//        @Valid @RequestParam("mail") String mail,
//        @Valid @RequestParam("name") String name,
//        @Valid @RequestParam("login") String login,
//        @Valid @RequestParam("password") String password){
//
//        User user = new User();
//        user.setMail(mail);
//        user.setName(name);
//        user.setLogin(login);
//        user.setPassword(password);
//        ur.save(user);
//        return "redirect:/login";
//    }
}