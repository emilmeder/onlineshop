package kz.attractor.month9onlineshop.dto;

import kz.attractor.month9onlineshop.model.*;
import lombok.*;
@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDto {

    private int id;
    private String name;
    private String login;
    private String password;
    private String mail;


    public static UserDto from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .login(user.getLogin())
                .password(user.getPassword())
                .mail(user.getMail())
                .build();
    }
}