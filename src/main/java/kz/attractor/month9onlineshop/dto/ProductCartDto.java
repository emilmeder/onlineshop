package kz.attractor.month9onlineshop.dto;
import kz.attractor.month9onlineshop.model.*;
import lombok.*;
@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductCartDto {
    private int id;
}
