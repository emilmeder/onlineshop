package kz.attractor.month9onlineshop.dto;

import kz.attractor.month9onlineshop.model.*;
import lombok.*;
@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDto {

    private int id;
    private String name;
    private String image;
    private String description;
    private double price;
    private int qty;

    public static ProductDto from(Product product) {
        return builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .image(product.getImage())
                .price(product.getPrice())
                .qty(product.getQty())
                .build();
    }

    private static String calcStoreImagePath(Product product) {
        if (!product.getImage().isBlank()) {
            return product.getImage();
        }
        return String.format("/images/store%d.png", Math.abs(product.getName().hashCode() % 4));
    }
}