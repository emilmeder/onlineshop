package kz.attractor.month9onlineshop.dto;
import kz.attractor.month9onlineshop.model.*;
import lombok.*;
@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TypeDto {

    private int id;
    private String name;
    private String icon;


    public static TypeDto from(Type type) {
        return builder()
                .id(type.getId())
                .name(type.getName())
                .icon(type.getIcon())
                .build();
    }
}