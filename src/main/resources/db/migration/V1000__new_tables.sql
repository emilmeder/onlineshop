use `onlineshop`;

create table `type`
(
    `id`   INT auto_increment NOT NULL,
    `name` varchar(128)       NOT NULL,
    `icon` varchar(128)       NOT NULL,
    PRIMARY KEY (`id`)
);

create table `user`
(
    `id`   INT auto_increment NOT NULL,
    `mail` varchar(128)       NOT NULL,
    `login` varchar(128)       NOT NULL,
    `name`  varchar(128)       NOT NULL,
    `password` varchar(128)    NOT NULL,
    PRIMARY KEY (`id`)
);

create table `product`
(
    `id`              INT auto_increment NOT NULL,
    `name`            varchar(128)       NOT NULL,
    `image`           varchar(128)       NOT NULL,
    `description`     varchar(128)       NOT NULL,
    `qty`             INTEGER            NOT NULL,
    `price`           double             not null,
    `type_id` int                not null,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_type`
        FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
);

create table `customers` (
                             `id` int auto_increment NOT NULL,
                             `email` varchar(128) NOT NULL,
                             `password` varchar(128) NOT NULL,
                             `fullname` varchar(128) NOT NULL default ' ',
                             `enabled` boolean NOT NULL default true,
                             `role` varchar(16) NOT NULL default 'USER',
                             PRIMARY KEY (`id`),
                             UNIQUE INDEX `email_unique` (`email` ASC)
);

CREATE TABLE `cart`(
                       `id`           INT          NOT NULL AUTO_INCREMENT,
                       `customers_id` INT(11)      NULL,
                       `session`      VARCHAR(128) NOT NULL,
                       PRIMARY KEY (`id`),
                       CONSTRAINT `fk_cart`
                           FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `product_cart`(
                               `id`           INT          NOT NULL AUTO_INCREMENT,
                               `product_id` INT(11) NOT NULL,
                               `cart_id`     INT     NOT NULL,
                               `qty`         INT     NOT NULL,
                               PRIMARY KEY (`id`),
                               CONSTRAINT `fk_product_cart_1`
                                   FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
                               CONSTRAINT `fk_product_cart_2`
                                   FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
);

CREATE TABLE `resets`(
                         `id`           INT          NOT NULL AUTO_INCREMENT,
                         `customer_id` INT(11)      NULL,
                         `token`      VARCHAR(128) NOT NULL,
                         PRIMARY KEY (`id`),
                         CONSTRAINT `fk_resets`
                             FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `review`(
                         `id`           INT          NOT NULL AUTO_INCREMENT,
                         `customer_id` INT(11)      NULL,
                         `product_id` INT(11)      NULL,
                         `text`      VARCHAR(400) NOT NULL,
                         PRIMARY KEY (`id`),
                         CONSTRAINT `fk_review_1`
                             FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
                         CONSTRAINT `fk_review_2`
                             FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
);
