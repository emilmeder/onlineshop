use `onlineshop`;

insert into `user` (`name`,`login`,`password`,`mail`) values
('Johnny3', 'Johnny3','qqw','Johnny3@gmail.com'),
('Charlie', 'Charlie','qqw','Charlie@gmail.com'),
('J-Dog', 'J-Dog','qqw','J-Dog@gmail.com');

insert into `type` (`name`, `icon`) values
('Male', 'Male.png'),
('Female', 'Female.png'),
('Unisex', 'Unisex.png');

insert into `product` (`name`, `image`, `description`, `qty`, `price`, `type_id`) values
('Roland-P', 'https://www.dhresource.com/0x0/f2/albu/g6/M00/B9/3B/rBVaR1p_BsGAW-xFAAU5JmkFNXg391.jpg', 'description1', 2, 2000, 1),
('Casio-S', 'https://www.dhresource.com/0x0/f2/albu/g6/M00/B9/3B/rBVaR1p_BsGAW-xFAAU5JmkFNXg391.jpg', 'description2', 2, 1500, 1),
('Sigma', 'https://www.dhresource.com/0x0/f2/albu/g6/M00/B9/3B/rBVaR1p_BsGAW-xFAAU5JmkFNXg391.jpg', 'description3', 2, 2500, 1),
('G-3', 'https://i.pinimg.com/originals/3d/71/10/3d7110cc3a0d3d3b226796262f9af9b0.jpg', 'description4', 2, 4000, 2),
('Taylor', 'https://i.pinimg.com/originals/3d/71/10/3d7110cc3a0d3d3b226796262f9af9b0.jpg', 'description5', 2, 2500, 2),
('Collins', 'https://i.pinimg.com/originals/3d/71/10/3d7110cc3a0d3d3b226796262f9af9b0.jpg', 'description6', 2, 2500, 2),
('Martin', 'https://5.imimg.com/data5/UT/OM/MY-46471958/corum-500x500.jpeg', 'description7', 2, 2500, 3),
('Marshall', 'https://5.imimg.com/data5/UT/OM/MY-46471958/corum-500x500.jpeg', 'description8', 2, 2500, 3),
('ATT-T', 'https://5.imimg.com/data5/UT/OM/MY-46471958/corum-500x500.jpeg', 'description9', 2, 3000, 3);
